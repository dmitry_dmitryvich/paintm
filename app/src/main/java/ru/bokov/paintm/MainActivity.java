package ru.bokov.paintm;

import android.graphics.Canvas;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    private CanvasView canvasView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        canvasView = (CanvasView) findViewById(R.id.canvas);
    }

    public void clearCanvas(View v) {
        canvasView.clearCanvas();
    }

    public void setEraser(View v) {
        canvasView.setEraser();
    }

    public void setPencil(View v) {
        canvasView.setPencil();
    }

}
